#include "TaskQueueThreadSafe.h"
#include"CLLog.h"
#include "CLCriticalSection.h"
TaskQueueThreadSafe::TaskQueueThreadSafe(unsigned int pMaxNum):TaskQueue(),_emptyEvent(true){
    //具体最大值取决于map本身的限制
    _max=pMaxNum<_tskqg.max_size()?pMaxNum:_tskqg.max_size();
}
TaskQueueThreadSafe::~TaskQueueThreadSafe(){}
bool TaskQueueThreadSafe::IsEmpty(){
    //CLCriticalSection cs(&_mutex);//临界区访问
    return _tskqg.empty();
}
bool TaskQueueThreadSafe::IsFull(){
    //CLCriticalSection cs(&_mutex);
    return _tskqg.size()>_max;

}
unsigned int TaskQueueThreadSafe::Length(){
    //CLCriticalSection cs(&_mutex);
    return _tskqg.size();
}
bool TaskQueueThreadSafe::PushTask(const shared_ptr<Task>& pTask,bool limit){
    try{
	CLCriticalSection cs(&_mutex);
	if(limit){
	    if(_tskqg.size()>=_max){
		CLLog::WriteLogMsg("In TaskQueueThreadSafe::PushTask,Out of maxsize",-1);
		return false;
	    }
	}
	_tskqg.insert(pair<unsigned int,shared_ptr<Task> >(pTask->GetID(),pTask));
    }
    catch(const char* str){
	CLLog::WriteLogMsg("In TaskQueueThreadSafe::PushTask error",-1);
	return false;
    }
    _emptyEvent.Set();//唤醒因队列为空而阻塞的线程,V操作,把这个放在外面是为了减少锁的力度
    return true;
}
//任务出队
shared_ptr<Task> TaskQueueThreadSafe::PopTask(){
    _emptyEvent.Wait();
    try{
	CLCriticalSection cs(&_mutex);
	if(_tskqg.empty()){
	    CLLog::WriteLogMsg("In TaskQueueThreadSafe::PopTask error,Queue is Empty",-1);
	    return NULL;
	}
	shared_ptr<Task> rtn=_tskqg.begin()->second;
	_tskqg.erase(_tskqg.begin());
	return rtn;
    }
    catch(const char*){
	CLLog::WriteLogMsg("In TaskQueueThreadSafe::PopTask error",-1);
	return NULL;
    }
}
