#ifndef TASKQUEUE_h
#define TASKQUEUE_h
/*任务队列抽象类
 * */
#include<memory>
#include "Task.h"
using namespace std;
class TaskQueue{
    public:
	TaskQueue();
	virtual ~TaskQueue();
	virtual bool IsEmpty()=0;
	virtual bool IsFull()=0;
	virtual unsigned int Length()=0;
	virtual bool PushTask(const shared_ptr<Task>&,bool)=0;
	virtual shared_ptr<Task> PopTask()=0;
};
#endif
