#include"ExcuterThread.h"
#include"CLLog.h"
ExcuterThread::ExcuterThread(shared_ptr<FunctionProvider> pFun,bool pdeatach):Excuter(pFun){
    _running=false;
    _detach=pdeatach;
}
ExcuterThread::~ExcuterThread(){}

CLStatus ExcuterThread::Run(void* pArgu){
    if(_running){
	CLLog::WriteLogMsg("Error:ExcuterThread is Running!Can't Run twice",-1);
	return CLStatus(-1,0);
    }
    _argu=pArgu;
    int r=pthread_create(&_tid,0,StartFunctionOfThread,this);
    if(r!=0){
	CLLog::WriteLogMsg("In ExcuterThread::Run() pthread_create error",r);
	return CLStatus(-1,0);
    }
    _running=true;
    return detach(_detach);
}
void* ExcuterThread::StartFunctionOfThread(void *pContext){
    ExcuterThread *exthread=(ExcuterThread *)pContext;
    exthread->_fun->Function(exthread->_argu);
}
CLStatus ExcuterThread::detach(bool pDetach){
    if(pDetach){
	int i=pthread_detach(_tid);
	if(i!=0){
	    CLLog::WriteLogMsg("In ExcuterThread::detach,pthread_detach error",i);
	    return CLStatus(-1,0);
	}
	return CLStatus(0,0);
    }
    return CLStatus(0,0);
}
CLStatus ExcuterThread::WaitForDeath(){
    if(_detach){
	CLLog::WriteLogMsg("In ExcuterThread::WaitForDeath error:the thread was already detached",-1);
	return CLStatus(-1,0);
    }
    int r=pthread_join(_tid,0);
    if(r!=0){
	CLLog::WriteLogMsg("In ExcuterThread::WaitForDeath pthread_join error",r);
	return CLStatus(-1,0);
    }
    return CLStatus(0,0);
}
