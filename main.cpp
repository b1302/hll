#include<iostream>
#include<memory>
#include<unistd.h>
#include"ExcuterThread.h"
#include"FunctionProvider.h"
#include "TaskQueueThreadSafe.h"
using namespace std;
class FunctionTest:public FunctionProvider{
    bool Function(void* p){
	cout<<(int)p<<endl;
    }
};
int main(){
    shared_ptr<FunctionProvider> fun=make_shared<FunctionTest>();
    shared_ptr<Excuter> ex=make_shared<ExcuterThread>(fun);
    ex->Run((void*)3);
    ex->WaitForDeath();
    shared_ptr<TaskQueueThreadSafe> tqg=make_shared<TaskQueueThreadSafe>();
    for(int i=1;i<100000;i++){
	tqg->PushTask(make_shared<Task>(rand()%10000,"http://www.baidu.com"));
    }
    while(!tqg->IsEmpty()){
	cout<<tqg->PopTask()->GetID()<<" ";
    }
    return 0;
}
