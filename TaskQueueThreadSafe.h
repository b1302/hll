#ifndef TASKQUEUEGLOBAL_H
#define TASKQUEUEGLOBAL_H
#include<map>
#include<memory>
#include "TaskQueue.h"
#include"CLEvent.h"
#include"CLMutex.h"
/*
 * 线程安全队列
 * 该任务队列是线程安全的。
 * */
using namespace std;
class TaskQueueThreadSafe:public TaskQueue{
    public:
	//默认队列中的最大数量为100000
	TaskQueueThreadSafe(unsigned int pMaxNum=100000);
	virtual ~TaskQueueThreadSafe();
	virtual bool IsEmpty();
	virtual bool IsFull();
	virtual unsigned int Length();
	//默认对队列的数目是有限制的
	virtual bool PushTask(const shared_ptr<Task>&,bool limit=true);
	virtual shared_ptr<Task> PopTask();
    private:
	map<unsigned int,shared_ptr<Task> > _tskqg;
	unsigned int _max;
	CLMutex _mutex;
	CLEvent _emptyEvent;
};
#endif
