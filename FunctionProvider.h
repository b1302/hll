#ifndef FUNCTIONPROVIDER_H
#define FUNCTIONPROVIDER_H
/*
 * 执行函数基类对象，执行体将执行该对象中的Function对象
 * 如果需要执行体执行函数，可以继承于该类。然后执行
 * */
class FunctionProvider{
    public:
	FunctionProvider(){};
	virtual ~FunctionProvider(){};
	virtual bool Function(void*)=0;
    private:
	FunctionProvider& operator=(const FunctionProvider&);
	FunctionProvider(const FunctionProvider&);
};
#endif
