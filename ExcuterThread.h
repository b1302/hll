#ifndef ExcuterThread_H
#define ExcuterThread_H
/*线程执行体，执行FunctionProvider对象中的函数
 * */
#include<memory>
#include<pthread.h>
#include "Excuter.h"
class ExcuterThread:public Excuter{
    public:
	/*第二个参数是线程detach的参数，默认是关闭的。
	 * ps.如果要设置默认参数得写在头文件中
	 * */
	explicit ExcuterThread(shared_ptr<FunctionProvider> pFun,bool pdeatach=false);
	virtual ~ExcuterThread();

	virtual CLStatus Run(void*);
	virtual CLStatus WaitForDeath();
    private:
	static void* StartFunctionOfThread(void *pContext);

	CLStatus detach(bool);
	pthread_t _tid;
	void* _argu;
	bool _running;
	bool _detach;
};
#endif
