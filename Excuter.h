#ifndef EXCUTER_H
#define EXCUTER_H
/*
 * 执行体对象
 * */
#include<memory>
#include "FunctionProvider.h"
#include "CLStatus.h"
using namespace std;
class Excuter{
    public:
	explicit Excuter(shared_ptr<FunctionProvider>);
	//explicit Excuter(shared_ptr<FunctionProvider>,bool);
	virtual ~Excuter();
	virtual CLStatus Run(void*)=0;//执行任务表中任务
	virtual CLStatus WaitForDeath()=0;//等待该执行器完成任务
    protected:
	shared_ptr<FunctionProvider> _fun;//执行函数
    private:
	Excuter(const Excuter&);
	Excuter& operator=(const Excuter&);
};
#endif
