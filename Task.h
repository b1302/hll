#ifndef TASK_H
#define TASK_H
/*任务类
 * */
#include<string>
using namespace std;
class Task{
    protected:
	unsigned int _taskid;
	string _task;
    public:
	explicit Task(unsigned int pid,string ptask){
	    _taskid=pid;
	    _task=ptask;
	}
	virtual ~Task(){};
	virtual unsigned int GetID(){
	    return _taskid;
	}
	virtual string GetTask(){
	    return _task;
	}
};
#endif
